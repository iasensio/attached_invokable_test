import QtQuick 2.0
import QtQuick.Window 2.0
import QtQuick.Layouts 1.0
import QtQuick.Controls 2.0 as QQC2

import test.attached 1.0

Window
{
    visible: true
    width: 300
    height: 240

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 6
        spacing: 10

        QQC2.Label {
            text: "Tests whether calling a Q_INVOKABLE method on an attached object works"
            Layout.fillWidth: true
            wrapMode: Text.WordWrap
        }

        QQC2.Button {
            text: "Increase Counter"
            onClicked: Counter.increase()
        }

        QQC2.Label {
            text: "Clicking counter: " + Counter.value
            font.bold: true
            font.pointSize: 20
        }
    }
}
