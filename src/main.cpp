#include <QGuiApplication>
#include <QQmlApplicationEngine>


class CounterAttachedBaseType: public QObject
{
    Q_OBJECT
    QML_ANONYMOUS
    Q_PROPERTY (int value READ value NOTIFY valueChanged)

public:
    int value() { return m_value; }
    Q_SIGNAL void valueChanged();

protected:
    int m_value = 0;
};


class CounterAttachedType: public CounterAttachedBaseType
{
    Q_OBJECT
    QML_ANONYMOUS

public:

    Q_INVOKABLE void increase() {
        qInfo() << "Called increase() method on CounterAttachedType";
        m_value ++;
        Q_EMIT valueChanged();
    }
};

class Counter : public QObject
{
    Q_OBJECT
    QML_ATTACHED(CounterAttachedBaseType)
    QML_NAMED_ELEMENT(Counter)

public:
    static CounterAttachedBaseType *qmlAttachedProperties(QObject *object)
    {
        static CounterAttachedType *s_attached = nullptr;
        if (!s_attached) {
            s_attached = new CounterAttachedType();
        }
        return s_attached;
    }
};


int main(int argc, char *argv[])
{
    qmlRegisterType<Counter>("test.attached", 1, 0, "Counter");

    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine(QUrl(QStringLiteral("qrc:/qml/main.qml")));

    return app.exec();
}

#include "main.moc"
