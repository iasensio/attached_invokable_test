# Attached Invokable Test

Tests whether calling a Q_INVOKABLE method on an attached object works

# Compile and Test

Execute `build/src/attached_invokable_test` and press the button. If calling to the method works, the counter will increase.
